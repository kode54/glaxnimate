<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="14"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="84"/>
        <source>Credits</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="132"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;h1 align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:12pt; font-weight:600;&quot;&gt;Links&lt;/span&gt;&lt;/h1&gt;
&lt;ul style=&quot;margin-top: 0px; margin-bottom: 0px; margin-left: 0px; margin-right: 0px; -qt-list-indent: 1;&quot;&gt;&lt;li style=&quot; margin-top:12px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://gitlab.com/mattbas/glaxnimate&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Code&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://gitlab.com/mattbas/glaxnimate/-/issues&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Issues&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
&lt;li style=&quot; margin-top:0px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;a href=&quot;https://t.me/Glaxnimate&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#2980b9;&quot;&gt;Chat&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;&lt;/ul&gt;
&lt;h1 align=&quot;center&quot; style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:12pt; font-weight:600;&quot;&gt;License&lt;/span&gt;&lt;/h1&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;Copyright (C) 2020  Mattia Basaglia&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;Glaxnimate is free software: you can redistribute it and/or modify&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;it under the terms of the GNU General Public License as published by&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;the Free Software Foundation, either version 3 of the License, or&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;(at your option) any later version.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;Glaxnimate is distributed in the hope that it will be useful,&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;but WITHOUT ANY WARRANTY; without even the implied warranty of&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;GNU General Public License for more details.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;You should have received a copy of the GNU General Public License&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;along with this program.  If not, see &amp;lt;&lt;/span&gt;&lt;a href=&quot;http://www.gnu.org/licenses/&quot;&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt; text-decoration: underline; color:#0057ae;&quot;&gt;http://www.gnu.org/licenses/&lt;/span&gt;&lt;/a&gt;&lt;span style=&quot; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&amp;gt;.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;Sans Serif&apos;; font-size:9pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="177"/>
        <source>Paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="198"/>
        <source>User Data Directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="214"/>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="217"/>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="246"/>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="249"/>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="278"/>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="281"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="230"/>
        <source>Settings File:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="262"/>
        <source>Backup Directory:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="294"/>
        <source>Data Directories:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="314"/>
        <source>Icon Theme Paths:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="339"/>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="357"/>
        <source>System</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="362"/>
        <source>Kernel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="367"/>
        <source>CPU</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="372"/>
        <source>Application Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="377"/>
        <source>System Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="382"/>
        <source>ZLib</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="387"/>
        <source>Potrace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="392"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="402"/>
        <source>About Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/about_dialog.ui" line="426"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorPickerWidget</name>
    <message>
        <location filename="../../src/gui/widgets/tools/color_picker_widget.ui" line="17"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/tools/color_picker_widget.ui" line="30"/>
        <source>Stroke</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ColorSelector</name>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="23"/>
        <source>Color Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="27"/>
        <source>HSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="32"/>
        <source>HSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="37"/>
        <source>RGB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="42"/>
        <source>CYMK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="47"/>
        <source>Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="74"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="111"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="151"/>
        <source>Saturation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="77"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="285"/>
        <source>S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="84"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="101"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="141"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="94"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="121"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="131"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="205"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="212"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="229"/>
        <source>Hue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="124"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="215"/>
        <source>H</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="144"/>
        <source>V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="292"/>
        <source>L</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="315"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="325"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="395"/>
        <source>Blue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="328"/>
        <source>B</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="335"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="355"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="385"/>
        <source>Green</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="345"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="365"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="375"/>
        <source>Red</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="348"/>
        <source>R</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="358"/>
        <source>G</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="421"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="431"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="441"/>
        <source>Cyan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="424"/>
        <source>C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="451"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="461"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="471"/>
        <source>Magenta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="454"/>
        <source>M</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="481"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="491"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="501"/>
        <source>Yellow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="484"/>
        <source>Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="511"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="521"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="531"/>
        <source>Black</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="514"/>
        <source>K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="560"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="570"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="580"/>
        <source>Alpha (Transparency)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="573"/>
        <source>A</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="624"/>
        <source>Primary Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="643"/>
        <source>Secondary Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="666"/>
        <location filename="../../src/gui/widgets/shape_style/color_selector.ui" line="669"/>
        <source>Swap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CompoundTimelineWidget</name>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.ui" line="125"/>
        <source>Add Keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="115"/>
        <source>Transition From Previous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="116"/>
        <source>Transition To Next</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="118"/>
        <source>Hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="121"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="124"/>
        <source>Ease</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="127"/>
        <source>Custom...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="130"/>
        <source>Remove Keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/compound_timeline_widget.cpp" line="173"/>
        <source>Update keyframe transition</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DocumentMetadataDialog</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/document_metadata_dialog.ui" line="14"/>
        <source>Document Metadata</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/document_metadata_dialog.ui" line="31"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/document_metadata_dialog.ui" line="36"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DocumentSwatchWidget</name>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="38"/>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="41"/>
        <source>Swatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="65"/>
        <source>Add color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="68"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="79"/>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="461"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="96"/>
        <source>Generate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="99"/>
        <source>Generate Swatch from the open Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="108"/>
        <source>Save Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.ui" line="117"/>
        <source>From Palette...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="40"/>
        <source>Gather Document Swatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="112"/>
        <source>Link Shapes to Swatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="236"/>
        <source>Modify Palette Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="315"/>
        <source>Load from Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="316"/>
        <source>No palettes are installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="322"/>
        <source>Swatch from Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="331"/>
        <source>Overwrite on save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="335"/>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="506"/>
        <source>Link shapes with matching colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="340"/>
        <source>Remove existing colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="359"/>
        <source>Load Palette</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="404"/>
        <source>Unlink Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="408"/>
        <source>Unlink fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="417"/>
        <source>Unlink stroke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="431"/>
        <source>Rename...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="435"/>
        <source>Rename Swatch Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="435"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="443"/>
        <source>Edit Color...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="470"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="486"/>
        <source>Set as fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/document_swatch_widget.cpp" line="495"/>
        <source>Set as stroke</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EnumCombo</name>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="45"/>
        <source>NonZero</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="47"/>
        <source>Even Odd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="55"/>
        <source>Butt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="57"/>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="69"/>
        <source>Round</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="59"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="67"/>
        <source>Miter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="71"/>
        <source>Bevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="79"/>
        <source>Star</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/enum_combo.cpp" line="81"/>
        <source>Polygon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FillStyleWidget</name>
    <message>
        <location filename="../../src/gui/widgets/shape_style/fill_style_widget.cpp" line="76"/>
        <source>Update Fill Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FrameControlsWidget</name>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="26"/>
        <source>Frame number</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="32"/>
        <source> f</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="39"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="53"/>
        <source>Loop animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="56"/>
        <source>Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="73"/>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="76"/>
        <source>Go To First Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="90"/>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="93"/>
        <source>Go To Previous Keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="104"/>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="107"/>
        <source>Go To Previous Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="118"/>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="121"/>
        <source>Go To Next Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="135"/>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="138"/>
        <source>Go To Next Keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="149"/>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="152"/>
        <source>Go To Last Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="163"/>
        <location filename="../../src/gui/widgets/timeline/frame_controls_widget.ui" line="166"/>
        <source>Record</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlaxnimateApp</name>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="85"/>
        <source>User Interface</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="87"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="87"/>
        <source>Interface Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="88"/>
        <source>Icon Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="93"/>
        <source>New Animation Defaults</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="95"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="96"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="97"/>
        <source>FPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="97"/>
        <source>Frames per second</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="98"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="98"/>
        <source>Duration in seconds</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="100"/>
        <source>Open / Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="101"/>
        <source>Max Recent Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="104"/>
        <source>Backup Frequency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="104"/>
        <source>How often to save a backup copy (in minutes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="106"/>
        <source>Scripting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="111"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GlaxnimateWindow</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="44"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="48"/>
        <source>Open &amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="75"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="80"/>
        <source>&amp;Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="84"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="803"/>
        <source>&amp;New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="102"/>
        <source>&amp;Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="131"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="878"/>
        <source>&amp;Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="156"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="160"/>
        <source>&amp;Views</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="174"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="183"/>
        <source>Plu&amp;gins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="238"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="747"/>
        <source>toolBar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="965"/>
        <source>&amp;Empty Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1187"/>
        <source>Cu&amp;t</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1361"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1370"/>
        <source>Solid &amp;Color Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1379"/>
        <source>&amp;Raster...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1388"/>
        <source>S&amp;VG...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1397"/>
        <source>&amp;Web Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1406"/>
        <source>Online &amp;Manual...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1415"/>
        <source>Report &amp;Issue...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="308"/>
        <source>Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="188"/>
        <source>&amp;Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="192"/>
        <source>Render Single &amp;Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="213"/>
        <source>&amp;Playback</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="274"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="501"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="323"/>
        <source>Tool Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="353"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="393"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="396"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="407"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="410"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="421"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="424"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="448"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="451"/>
        <source>Move To Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="462"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="465"/>
        <source>Move Up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="476"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="479"/>
        <source>Move Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="490"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="493"/>
        <source>Move To Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="518"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="559"/>
        <source>Timeline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="598"/>
        <source>Script Console</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="642"/>
        <source>Logs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="674"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="506"/>
        <source>Stroke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="708"/>
        <source>Undo History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="732"/>
        <source>Swatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="772"/>
        <source>Gradients</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="812"/>
        <source>&amp;Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="821"/>
        <source>&amp;Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="830"/>
        <source>Save &amp;As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="839"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="848"/>
        <source>&amp;Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="863"/>
        <source>&amp;Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="866"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="881"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="893"/>
        <source>&amp;Draw Bezier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="896"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="908"/>
        <source>Draw &amp;Freehand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="911"/>
        <source>F6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="923"/>
        <source>&amp;Rectangle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="926"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="938"/>
        <source>&amp;Ellipse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="941"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="953"/>
        <source>S&amp;tar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="956"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1451"/>
        <source>Validate &amp;Telegram Sticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1460"/>
        <source>Re&amp;size...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1469"/>
        <source>Donate...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1478"/>
        <source>&amp;Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1487"/>
        <source>&amp;Stroke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1496"/>
        <source>&amp;Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1508"/>
        <source>Open Most Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1517"/>
        <source>Import Image...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1526"/>
        <source>&amp;Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1538"/>
        <source>Make segments straight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1550"/>
        <source>Make segments curved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1559"/>
        <source>&amp;Export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1568"/>
        <source>E&amp;xport As...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1580"/>
        <source>Dissolve Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1589"/>
        <source>Cleanup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1592"/>
        <source>Remove unused assets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1601"/>
        <source>Timing...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1610"/>
        <source>&amp;Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1625"/>
        <source>&amp;Loop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1637"/>
        <source>&amp;Record Keyframes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1646"/>
        <source>Jump to &amp;Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1655"/>
        <source>Jump to &amp;End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1664"/>
        <source>&amp;Next Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1673"/>
        <source>Pre&amp;vious Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1682"/>
        <source>Metadata...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1690"/>
        <source>Trace Bitmap...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="986"/>
        <source>&amp;Precomposition Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="998"/>
        <source>&amp;Move To Composition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1010"/>
        <source>New &amp;Pre Composition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1022"/>
        <source>&amp;Union</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1034"/>
        <source>&amp;Difference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1046"/>
        <source>&amp;Intersect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1058"/>
        <source>E&amp;xclusion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1070"/>
        <source>Re&amp;verse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1082"/>
        <source>Object to &amp;Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1121"/>
        <source>&amp;Join Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1133"/>
        <source>S&amp;plit Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1145"/>
        <source>&amp;Cusp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1157"/>
        <source>&amp;Smooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1169"/>
        <source>Sy&amp;mmetric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1178"/>
        <source>&amp;Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="974"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1424"/>
        <source>&amp;Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1433"/>
        <source>&amp;Ungroup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1442"/>
        <source>&amp;Move to...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1196"/>
        <source>&amp;Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1094"/>
        <source>Add &amp;Node...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1106"/>
        <source>&amp;Delete Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1109"/>
        <source>Delete Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1208"/>
        <source>&amp;Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1217"/>
        <source>Raise to &amp;Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1220"/>
        <source>Home</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1229"/>
        <source>&amp;Raise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1232"/>
        <source>PgUp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1241"/>
        <source>&amp;Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1244"/>
        <source>PgDown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1253"/>
        <source>Lower to &amp;Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1256"/>
        <source>End</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1265"/>
        <source>Re&amp;load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1277"/>
        <source>&amp;Undo %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1289"/>
        <source>&amp;Redo %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1298"/>
        <source>&amp;Fit Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1307"/>
        <source>Re&amp;set View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1316"/>
        <source>&amp;Reset Rotation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1325"/>
        <source>Zoom &amp;In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1334"/>
        <source>Zoom &amp;Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1343"/>
        <source>Reset &amp;Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.ui" line="1352"/>
        <source>Pr&amp;eferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="15"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="37"/>
        <source>New Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="43"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="51"/>
        <source>File saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="45"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="53"/>
        <source>Could not save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="59"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="67"/>
        <source>File exported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="61"/>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="69"/>
        <source>Could not export file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/glaxnimate_window.cpp" line="182"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="90"/>
        <source>Pause</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="94"/>
        <source>Play</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="244"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="292"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="297"/>
        <source>Open File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="245"/>
        <source>Save File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="550"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="559"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="566"/>
        <source>Trace Bitmap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="550"/>
        <source>Only select one image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="559"/>
        <source>You need to select an image to trace</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_ui.cpp" line="566"/>
        <source>You selected an image with no data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_script.cpp" line="102"/>
        <source>Could not run the plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_script.cpp" line="105"/>
        <source>Plugin raised an exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_script.cpp" line="113"/>
        <source>Could not find an interpreter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="80"/>
        <source>Export...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="144"/>
        <source>Looks like this file is being edited by another Glaxnimate instance or it was being edited when Glaxnimate crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="150"/>
        <source>Close Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="157"/>
        <source>Load Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="247"/>
        <source>Export to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="292"/>
        <source>No importer found for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="297"/>
        <source>The file might have been moved or deleted
%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="310"/>
        <source>No file to reload from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="336"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="343"/>
        <source>Web Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="336"/>
        <source>Could not create file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="343"/>
        <source>Could not open browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="351"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="375"/>
        <source>Save Frame Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="354"/>
        <source>Frame%1.png</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="361"/>
        <source>Image files (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="368"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="389"/>
        <source>Render Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="368"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="389"/>
        <source>Could not save image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="378"/>
        <source>Frame%1.svg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="381"/>
        <source>Scalable Vector Graphics (*.svg)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="400"/>
        <source>Validate Telegram Sticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="402"/>
        <source>No issues found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="402"/>
        <source>Some issues detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="452"/>
        <source>Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="452"/>
        <source>Cannot load backup of a closed file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="515"/>
        <source>Unlink %1 Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="524"/>
        <source>Link %1 Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="146"/>
        <location filename="../../src/gui/tools/select_tool.cpp" line="370"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="161"/>
        <location filename="../../src/gui/tools/select_tool.cpp" line="362"/>
        <source>Cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="204"/>
        <source>Nothing to paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="210"/>
        <location filename="../../src/gui/tools/select_tool.cpp" line="366"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="317"/>
        <source>Move Shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="344"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="352"/>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="357"/>
        <source>Import Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="352"/>
        <source>Could not import image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="446"/>
        <source>Cleanup Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_model.cpp" line="454"/>
        <source>Removed %1 assets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="352"/>
        <source>Undo %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="357"/>
        <source>Redo %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="364"/>
        <source>Copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="375"/>
        <source>Group Shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="378"/>
        <source>Ungroup Shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="382"/>
        <source>Move to...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GradientListWidget</name>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="35"/>
        <source>Add Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="38"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="49"/>
        <source>Add from Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="52"/>
        <source>Preset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="75"/>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.cpp" line="188"/>
        <source>Remove Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="78"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="93"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="106"/>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="109"/>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="176"/>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="179"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="126"/>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="129"/>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="156"/>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="159"/>
        <source>Radial</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.ui" line="143"/>
        <source>Stroke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.cpp" line="111"/>
        <source>Set %1 Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.cpp" line="200"/>
        <source>Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/gradient_list_widget.cpp" line="334"/>
        <source>Gradient Presets</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IoStatusDialog</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/io_status_dialog.ui" line="14"/>
        <source>IoStatusDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/io_status_dialog.ui" line="43"/>
        <source>/path/to/your/file/in.here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/io_status_dialog.ui" line="77"/>
        <source>Issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/io_status_dialog.cpp" line="62"/>
        <source>Operation Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyframeEditorWidget</name>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="30"/>
        <source>Presets</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="36"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="44"/>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="88"/>
        <source>Hold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="49"/>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="93"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="54"/>
        <source>Ease In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="59"/>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="103"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="80"/>
        <source>Finish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/timeline/keyframe_editor_widget.ui" line="98"/>
        <source>Ease Out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NodeMenu</name>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="25"/>
        <source>ResetTransform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="53"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="77"/>
        <source>Move to Top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="81"/>
        <source>Raise</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="85"/>
        <source>Lower</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="89"/>
        <source>Move to Bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="147"/>
        <source>Convert %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="175"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="179"/>
        <source>Duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="186"/>
        <source>Reset Transform</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="208"/>
        <source>Parent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="209"/>
        <source>Convert to Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="213"/>
        <source>Convert to Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="219"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="221"/>
        <source>Embed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="226"/>
        <source>Open with External Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="230"/>
        <source>Could not find suitable application, check your system settings.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="235"/>
        <source>From File...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="237"/>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="241"/>
        <source>Update Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="256"/>
        <source>Trace Bitmap...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/node_menu.cpp" line="198"/>
        <source>Move to...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PluginSettingsWidget</name>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="24"/>
        <source>Reload Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="27"/>
        <source>Refresh</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="51"/>
        <source>Install from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="54"/>
        <source>Install...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="83"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="90"/>
        <source>Author</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="97"/>
        <source>123</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="107"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="124"/>
        <source>Services</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="143"/>
        <source>Enable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="154"/>
        <source>Disable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="165"/>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="194"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.ui" line="199"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.cpp" line="54"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.cpp" line="56"/>
        <source>Menu Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/plugin_settings_widget.cpp" line="58"/>
        <source>File Format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../../src/core/utils/gzip.cpp" line="88"/>
        <source>ZLib %1%2 returned %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="12"/>
        <source>Informational Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="13"/>
        <source>Show this help and exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="14"/>
        <source>Show version information and exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="16"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="17"/>
        <source>File to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="19"/>
        <source>GUI Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="20"/>
        <source>If present, doen&apos;t restore the main window state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="23"/>
        <source>Use a specific size for the main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/main.cpp" line="31"/>
        <source>Print the window id</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMenu</name>
    <message>
        <location filename="../../src/gui/tools/handle_menu.hpp" line="26"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/handle_menu.hpp" line="38"/>
        <source>Remove Keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/handle_menu.hpp" line="51"/>
        <source>Add Keyframe</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/import_export_dialog.hpp" line="31"/>
        <source>Save file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/import_export_dialog.hpp" line="50"/>
        <source>Overwrite File?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/import_export_dialog.hpp" line="51"/>
        <source>The file &quot;%1&quot; already exists. Do you wish to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/import_export_dialog.hpp" line="71"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/import_export_dialog.hpp" line="89"/>
        <source>%1 Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/base.hpp" line="149"/>
        <source>%1 (%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/import_export_dialog.hpp" line="141"/>
        <source>All files (%1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="190"/>
        <source>Closing Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/gw_impl_document.cpp" line="191"/>
        <source>The animation has unsaved changes.
Do you want to save your changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/property_commands.hpp" line="18"/>
        <location filename="../../src/core/command/animation_commands.cpp" line="236"/>
        <source>Update %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/object_list_commands.hpp" line="20"/>
        <source>Create %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/object_list_commands.hpp" line="48"/>
        <location filename="../../src/core/command/object_list_commands.hpp" line="54"/>
        <source>Remove %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/object_list_commands.hpp" line="88"/>
        <source>Move Object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/app_info.cpp" line="8"/>
        <source>Glaxnimate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/rectangle_tool.hpp" line="14"/>
        <source>Rectangle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/rectangle_tool.hpp" line="15"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/rectangle_tool.hpp" line="34"/>
        <source>Draw Rectangle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/draw_tool_base.hpp" line="89"/>
        <source>%1 Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/draw_tool_base.hpp" line="112"/>
        <source>%1 Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/draw_tool_base.hpp" line="124"/>
        <source>%1 Stroke</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/graphics/main_composition_item.hpp" line="99"/>
        <source>Resize Canvas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/animation_commands.cpp" line="10"/>
        <source>Update %1 keyframe at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/animation_commands.cpp" line="110"/>
        <source>Remove %1 keyframe at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/animation_commands.cpp" line="231"/>
        <source>Add keyframe for %1 at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/animation_commands.cpp" line="234"/>
        <source>Update %1 at %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/animation_commands.cpp" line="246"/>
        <source>Update keyframe transition</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/animation_commands.cpp" line="297"/>
        <source>Move keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="21"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="22"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/ellipse_tool.cpp" line="11"/>
        <source>Ellipse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/ellipse_tool.cpp" line="12"/>
        <source>F5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/ellipse_tool.cpp" line="20"/>
        <source>Draw Ellipse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.hpp" line="21"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.hpp" line="22"/>
        <source>F2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/draw_tool.hpp" line="14"/>
        <source>Draw Bezier</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/draw_tool.hpp" line="15"/>
        <source>F3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/freehand.cpp" line="12"/>
        <location filename="../../src/gui/tools/freehand.cpp" line="39"/>
        <source>Draw Freehand</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/freehand.cpp" line="13"/>
        <source>F6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/star_tool.cpp" line="13"/>
        <source>Star</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/star_tool.cpp" line="14"/>
        <source>*</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/star_tool.cpp" line="45"/>
        <source>Draw Star</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="839"/>
        <source>Missing layer type for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="920"/>
        <source>Unsupported layer type %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="864"/>
        <source>Cannot use %1 as parent as it couldn&apos;t be loaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="874"/>
        <source>Invalid parent layer %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="945"/>
        <source>Missing shape type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="955"/>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="965"/>
        <source>Unsupported shape type %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="986"/>
        <source>Unknown field %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1193"/>
        <source>Invalid bezier point %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1218"/>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1225"/>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1232"/>
        <source>Invalid value for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1240"/>
        <source>Invalid keyframes for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1267"/>
        <source>Cannot load keyframe at %1 for %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/settings/clipboard_settings.hpp" line="11"/>
        <source>Clipboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/svg/svg_mime.hpp" line="15"/>
        <source>SVG</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/raster/raster_mime.hpp" line="17"/>
        <source>Raster Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/mime/json_mime.hpp" line="12"/>
        <source>JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/draw_tool.cpp" line="24"/>
        <source>Draw Shape</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/shape_commands.cpp" line="163"/>
        <source>Duplicate %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/structure_commands.cpp" line="66"/>
        <source>Move %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/shape_commands.cpp" line="106"/>
        <source>Group Shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/command/shape_commands.cpp" line="139"/>
        <source>Ungroup Shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="151"/>
        <source>Drag nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="220"/>
        <source>Cusp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="224"/>
        <source>Smooth</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="228"/>
        <source>Symmetrical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="235"/>
        <source>Set %1 Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="249"/>
        <source>Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="259"/>
        <source>Remove Node</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="263"/>
        <source>Show Tangents</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="270"/>
        <source>Remove Tangent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="444"/>
        <source>Dissolve Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="444"/>
        <source>Delete Nodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="793"/>
        <source>Set node type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="821"/>
        <source>Straighten segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="869"/>
        <source>Curve segments</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/color_picker.cpp" line="13"/>
        <source>Color Picker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/color_picker.cpp" line="14"/>
        <source>F7</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/settings/plugin_settings_group.hpp" line="17"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ResizeDialog</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.ui" line="14"/>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.cpp" line="19"/>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.cpp" line="71"/>
        <source>Resize Document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.ui" line="26"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.ui" line="49"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.ui" line="64"/>
        <source>Lock Apect Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.ui" line="86"/>
        <source>Scale Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.ui" line="96"/>
        <source>Preserve Layer Aspect Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/resize_dialog.cpp" line="75"/>
        <source>Resize</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShapeParentDialog</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/shape_parent_dialog.ui" line="14"/>
        <source>Destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/shape_parent_dialog.ui" line="41"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/shape_parent_dialog.ui" line="52"/>
        <source>Move</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShapeToolWidget</name>
    <message>
        <location filename="../../src/gui/widgets/tools/shape_tool_widget.ui" line="31"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/tools/shape_tool_widget.ui" line="17"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/tools/shape_tool_widget.ui" line="45"/>
        <source>Stroke</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Spin2D</name>
    <message>
        <location filename="../../src/gui/widgets/spin2d.cpp" line="71"/>
        <source>Lock Ratio</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StrokeStyleWidget</name>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="33"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="39"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="52"/>
        <source>Stroke Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="62"/>
        <source>Cap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="71"/>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="74"/>
        <source>Butt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="101"/>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="104"/>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="189"/>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="192"/>
        <source>Round</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="131"/>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="134"/>
        <source>Square</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="150"/>
        <source>Join</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="159"/>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="162"/>
        <source>Bevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="219"/>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="222"/>
        <source>Miter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="256"/>
        <source>Miter Limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="272"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.ui" line="297"/>
        <source>Dashes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/shape_style/stroke_style_widget.cpp" line="108"/>
        <source>Update Stroke Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimingDialog</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="14"/>
        <source>Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="22"/>
        <source>FPS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="45"/>
        <source>Duration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="58"/>
        <source>&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="65"/>
        <source>Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="84"/>
        <source>Layers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="90"/>
        <source>Keep Initial Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="97"/>
        <source>Trim Extra Frames</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.ui" line="110"/>
        <source>Scale Timing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/timing_dialog.cpp" line="70"/>
        <source>Change Animation Properties</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TraceDialog</name>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="14"/>
        <source>Trace Bitmap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="37"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="45"/>
        <source>Transparency</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="50"/>
        <source>Closest Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="55"/>
        <source>Extract Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="60"/>
        <source>Pixel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="70"/>
        <source>Image Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="89"/>
        <source>Output Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="113"/>
        <source>Invert</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="127"/>
        <source>Threshold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="154"/>
        <source>Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="176"/>
        <source>Add Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="179"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="203"/>
        <source>Remove Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="206"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="221"/>
        <source>Detect Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="244"/>
        <source>Tolerance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="258"/>
        <source>Outline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="285"/>
        <source>Curve Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="291"/>
        <source>Smoothness</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="304"/>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="396"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="330"/>
        <source>Minimum Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="343"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="365"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="389"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.ui" line="427"/>
        <source>Help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/dialogs/trace_dialog.cpp" line="278"/>
        <source>Traced %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewTransformWidget</name>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="29"/>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="32"/>
        <source>Fit View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="43"/>
        <source>Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="53"/>
        <source>%</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="72"/>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="75"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="86"/>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="89"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="100"/>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="103"/>
        <source>Reset Zoom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="114"/>
        <source>Angle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="124"/>
        <source>°</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="143"/>
        <location filename="../../src/gui/widgets/view_transform_widget.ui" line="146"/>
        <source>Reset Rotation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WindowMessageWidget</name>
    <message>
        <location filename="../../src/gui/widgets/window_message_widget.ui" line="87"/>
        <source>Hide</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>app::settings::Settings</name>
    <message>
        <location filename="../../src/gui/glaxnimate_app.cpp" line="16"/>
        <source>Glaxnimate Default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>graphics::BezierItem</name>
    <message>
        <location filename="../../src/gui/graphics/bezier_item.cpp" line="323"/>
        <source>Remove Point</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/graphics/bezier_item.cpp" line="343"/>
        <source>Update shape</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>graphics::GradientEditor</name>
    <message>
        <location filename="../../src/gui/graphics/gradient_editor.cpp" line="99"/>
        <source>Drag Gradient</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>graphics::PointItem</name>
    <message>
        <location filename="../../src/gui/graphics/bezier_item.cpp" line="210"/>
        <source>Remove node tangent</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>graphics::TransformGraphicsItem</name>
    <message>
        <location filename="../../src/gui/graphics/transform_graphics_item.cpp" line="355"/>
        <location filename="../../src/gui/graphics/transform_graphics_item.cpp" line="400"/>
        <source>Drag anchor point</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>io::ImportExport</name>
    <message>
        <location filename="../../src/core/io/base.cpp" line="16"/>
        <source>%1 (%2)</source>
        <extracomment>Open/Save file dialog file filter eg: &quot;Text files (.txt)&quot;</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>io::glaxnimate::GlaxnimateFormat</name>
    <message>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_mime.hpp" line="15"/>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_format.hpp" line="19"/>
        <source>Glaxnimate Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="41"/>
        <source>Property %1 of %2 refers to unexisting object %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="50"/>
        <source>Could not load %1 for %2: uuid refers to an unacceptable object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="61"/>
        <source>Object %1 is invalid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="108"/>
        <source>Wrong object type: expected &apos;%1&apos; but got &apos;%2&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="113"/>
        <source>Could not load %1 for %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="124"/>
        <source>Could not set property %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="150"/>
        <source>Item %1 for %2 in %3 isn&apos;t an object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="162"/>
        <source>Item %1 for %2 in %3 is not acceptable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="199"/>
        <source>Keyframe must specify a time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="204"/>
        <source>Keyframe must specify a value</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="214"/>
        <source>Could not add keyframe</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="380"/>
        <source>Objects of type &apos;MainComposition&apos; can only be at the top level of the document</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/import_state.hpp" line="387"/>
        <source>Unknow object of type &apos;%1&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_mime.cpp" line="76"/>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_importer.cpp" line="13"/>
        <source>Could not parse JSON: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_mime.cpp" line="82"/>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_importer.cpp" line="19"/>
        <source>No JSON object found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_importer.cpp" line="30"/>
        <source>Missing animation object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/glaxnimate/glaxnimate_importer.cpp" line="38"/>
        <source>Opening a file from a newer version of Glaxnimate</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>io::lottie::LottieFormat</name>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.hpp" line="15"/>
        <source>Lottie Animation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.hpp" line="22"/>
        <source>Pretty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.hpp" line="22"/>
        <source>Pretty print the JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="691"/>
        <source>Lottie only supports layers in the top level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="732"/>
        <source>Images cannot be grouped with other shapes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1342"/>
        <source>Could not parse JSON: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/lottie_format.cpp" line="1348"/>
        <source>No JSON object found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>io::lottie::LottieHtmlFormat</name>
    <message>
        <location filename="../../src/core/io/lottie/lottie_html_format.hpp" line="14"/>
        <source>Lottie HTML Preview</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>io::lottie::TgsFormat</name>
    <message>
        <location filename="../../src/core/io/lottie/tgs_format.hpp" line="11"/>
        <source>Telegram Animated Sticker</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/tgs_format.cpp" line="58"/>
        <source>Invalid height: %1, should be 512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/tgs_format.cpp" line="54"/>
        <source>Invalid width: %1, should be 512</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/tgs_format.cpp" line="34"/>
        <source>Star Shapes are not officially supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/tgs_format.cpp" line="37"/>
        <source>Images are not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/tgs_format.cpp" line="62"/>
        <source>Invalid fps: %1, should be 30 or 60</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/io/lottie/tgs_format.cpp" line="93"/>
        <source>File too large: %1k, should be under 64k</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>io::raster::RasterFormat</name>
    <message>
        <location filename="../../src/core/io/raster/raster_format.hpp" line="18"/>
        <source>Raster Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>io::svg::SvgFormat</name>
    <message>
        <location filename="../../src/core/io/svg/svg_format.hpp" line="14"/>
        <source>Scalable Vector Graphics</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>item_models::DocumentNodeModel</name>
    <message>
        <location filename="../../src/gui/item_models/document_node_model.cpp" line="338"/>
        <source>Move Layers</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>item_models::GradientListModel</name>
    <message>
        <location filename="../../src/gui/item_models/gradient_list_model.cpp" line="159"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/item_models/gradient_list_model.cpp" line="163"/>
        <source>Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/item_models/gradient_list_model.cpp" line="167"/>
        <source>#</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/item_models/gradient_list_model.cpp" line="169"/>
        <source>Number of users</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>item_models::PropertyModel</name>
    <message>
        <location filename="../../src/gui/item_models/property_model.cpp" line="577"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/item_models/property_model.cpp" line="579"/>
        <source>Value</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::AnimatableBase</name>
    <message>
        <location filename="../../src/core/model/animation/animatable.cpp" line="39"/>
        <source>Update %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Bitmap</name>
    <message>
        <location filename="../../src/core/model/defs/bitmap.hpp" line="32"/>
        <source>Bitmap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/model/defs/bitmap.cpp" line="153"/>
        <source>Embedded image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Ellipse</name>
    <message>
        <location filename="../../src/core/model/shapes/ellipse.hpp" line="25"/>
        <source>Ellipse</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Fill</name>
    <message>
        <location filename="../../src/core/model/shapes/fill.hpp" line="41"/>
        <source>Fill</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Gradient</name>
    <message>
        <location filename="../../src/core/model/defs/gradient.cpp" line="168"/>
        <source>%1 Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/model/defs/gradient.cpp" line="222"/>
        <source>Linear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/model/defs/gradient.cpp" line="224"/>
        <source>Radial</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::GradientColors</name>
    <message>
        <location filename="../../src/core/model/defs/gradient.cpp" line="38"/>
        <source>Gradient</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/model/defs/gradient.cpp" line="88"/>
        <source>Add color to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/model/defs/gradient.cpp" line="107"/>
        <source>Remove color from %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Group</name>
    <message>
        <location filename="../../src/core/model/shapes/group.hpp" line="45"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Image</name>
    <message>
        <location filename="../../src/core/model/shapes/image.hpp" line="28"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Layer</name>
    <message>
        <location filename="../../src/core/model/shapes/layer.hpp" line="73"/>
        <source>Layer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::MainComposition</name>
    <message>
        <location filename="../../src/core/model/main_composition.hpp" line="24"/>
        <source>Animation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::NamedColor</name>
    <message>
        <location filename="../../src/core/model/defs/named_color.cpp" line="11"/>
        <source>Unnamed Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Object</name>
    <message>
        <location filename="../../src/core/model/object.hpp" line="82"/>
        <source>Uknown Object</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Path</name>
    <message>
        <location filename="../../src/core/model/shapes/path.hpp" line="32"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::PolyStar</name>
    <message>
        <location filename="../../src/core/model/shapes/polystar.hpp" line="42"/>
        <source>PolyStar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Rect</name>
    <message>
        <location filename="../../src/core/model/shapes/rect.hpp" line="25"/>
        <source>Rectangle</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Stroke</name>
    <message>
        <location filename="../../src/core/model/shapes/stroke.hpp" line="60"/>
        <source>Stroke</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::Transform</name>
    <message>
        <location filename="../../src/core/model/transform.hpp" line="23"/>
        <source>Transform</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>model::detail::AnimatedPropertyBezier</name>
    <message>
        <location filename="../../src/core/model/animation/animatable_path.cpp" line="19"/>
        <source>Split Segment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/core/model/animation/animatable_path.cpp" line="61"/>
        <source>Remove Nodes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>tools::EditTool</name>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="288"/>
        <source>Gradient Colors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="305"/>
        <source>Stop Color...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="317"/>
        <source>Add Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/gui/tools/edit_tool.cpp" line="321"/>
        <source>Remove Stop</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>tools::SelectTool</name>
    <message>
        <location filename="../../src/gui/tools/select_tool.cpp" line="105"/>
        <source>Drag</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
