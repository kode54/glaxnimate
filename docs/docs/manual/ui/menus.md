Authors: Mattia Basaglia

# Menus

## File

![file menu](/img/screenshots/menus/file.png)

The usual File menu actions.

## Edit

![edit menu](/img/screenshots/menus/edit.png)

## Tools

![tools menu](/img/screenshots/menus/tools.png)

This menu toggles which is the active [tool](tools.md).

## View

![view menu](/img/screenshots/menus/view.png)

### Views

![views menu](/img/screenshots/menus/views.png)

This menu toggles which [views](docks.md) are visible.


## Document

![document menu](/img/screenshots/menus/document.png)

### Single frame

![render single frame menu](/img/screenshots/menus/render_single_frame.png)

### Web preview

![web preview menu](/img/screenshots/menus/render_single_frame.png)

The actions in this menu will open your browser to an html page that embeds
the animation using the exporting options associated with said action.

## Playback

![playback menu](/img/screenshots/menus/playback.png)

## Layers

![layers menu](/img/screenshots/menus/layers.png)

### New layer

![new layer menu](/img/screenshots/menus/new_layer.png)

## Object

![object menu](/img/screenshots/menus/object.png)

### Align

![align menu](/img/screenshots/menus/align.png)

## Path

![path menu](/img/screenshots/menus/path.png)

## Help

![help menu](/img/screenshots/menus/help.png)
