#pragma once

#include <array>
#include <QPointF>

namespace math::bezier {


using BezierSegment = std::array<QPointF, 4>;


} // namespace math::bezier
