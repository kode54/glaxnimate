#pragma once

namespace graphics {

enum ItemData
{
    NodePointer,
    HandleRole,
    GradientStopIndex,
    AssociatedProperty
};

} // namespace graphics
