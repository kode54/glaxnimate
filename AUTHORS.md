Developers
==========

Mattia "Glax" Basaglia <glaxnimate@mattbas.org> (Maintainer)


Translators
===========


English
-------

* Mattia "Glax" Basaglia

German
------

* Sythelux
